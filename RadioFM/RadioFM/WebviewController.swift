//
//  WebviewController.swift
//  RadioFM
//
//  Created by GOVIND KUMAR on 29/07/23.
//

import UIKit
import WebKit
import AVFoundation

class WebviewController: UIViewController {
    @IBOutlet weak var webVIew: WKWebView!
    @IBOutlet weak var buttonPlayPause: UIButton!
    var aStation: Station?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var buttonTag = true {
        didSet {
            buttonPlayPause.setImage(buttonTag ? UIImage(named: "Pause") : UIImage(named: "play"),
                                     for: .normal)
            buttonTag ? appDelegate.aAVPlayer?.play() : appDelegate.aAVPlayer?.pause()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = URL(string: aStation?.pageviewer ?? "") {
            webVIew.load(URLRequest(url: url))
        }
        buttonTag = true
        
       
        appDelegate.loadPlayer(url: aStation?.streamURL ?? "")
        
        
    }
    
    @IBAction func actionOnHome(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnSocial(_ sender: Any) {
        moveToSocial()
    }
   
    @IBAction func actionOnPlayPause(_ sender: Any) {
        buttonTag = !buttonTag
    }
    
    func moveToSocial() {
        if let viewController = self.storyboard?.instantiateViewController(identifier: "SocialviewController") as? SocialviewController {
            viewController.aStation = self.aStation
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
