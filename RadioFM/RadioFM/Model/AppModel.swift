//
//  AppModel.swift
//  RadioFM
//
//  Created by GOVIND KUMAR on 29/07/23.
//

import Foundation

// MARK: - Welcome
struct AppModel: Codable {
    let splash: Splash
    let stations: [Station]
    let ads: Ads
    let banners: [Banner]

    enum CodingKeys: String, CodingKey {
        case splash = "SPLASH"
        case stations = "STATIONS"
        case ads = "ADS"
        case banners = "BANNERS"
    }
}

// MARK: - Ads
struct Ads: Codable {
    let google: String
}

// MARK: - Banner
struct Banner: Codable {
    let img: String
    let link: String
    let time: String

    enum CodingKeys: String, CodingKey {
        case img = "IMG"
        case link = "LINK"
        case time = "TIME"
    }
}

// MARK: - Splash
struct Splash: Codable {
    let time: String
    let logo: String
    let bgcolor, bgimg: String

    enum CodingKeys: String, CodingKey {
        case time = "TIME"
        case logo = "LOGO"
        case bgcolor = "BGCOLOR"
        case bgimg = "BGIMG"
    }
}

// MARK: - Station
struct Station: Codable {
    let title: String
    let streamURL: String
    let pageviewer: String
    let logo: String
    let contatos: Contatos

    enum CodingKeys: String, CodingKey {
        case title = "TITLE"
        case streamURL
        case pageviewer = "Pageviewer"
        case logo = "Logo"
        case contatos = "Contatos"
    }
}

// MARK: - Contatos
struct Contatos: Codable {
    let promoções: String
    let whatsapp, telegram, instagram, facebook: String
    let threads, tiktok, twitter, youtube: String
    let eMail, dptoComercial, endereço: String

    enum CodingKeys: String, CodingKey {
        case promoções = "Promoções"
        case whatsapp = "WHATSAPP"
        case telegram = "TELEGRAM"
        case instagram = "INSTAGRAM"
        case facebook = "FACEBOOK"
        case threads = "THREADS"
        case tiktok = "TIKTOK"
        case twitter = "TWITTER"
        case youtube = "YOUTUBE"
        case eMail = "E-mail"
        case dptoComercial = "Dpto. Comercial"
        case endereço = "Endereço"
    }
}
