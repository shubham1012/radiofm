//
//  Loader.swift
//  CarWashServiceProvider
//
//  Created by GOVIND KUMAR on 18/05/23.
//

import UIKit

class Loader: UIView {
    
    @IBOutlet var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
   
    func showLoader() {
        Bundle.main.loadNibNamed("Loader", owner: self, options: nil)
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(self.contentView)
        contentView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.contentView.alpha = 0.0
        self.contentView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        UIView.animate(withDuration: 0.25) {
            self.contentView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.contentView.alpha = 1.0
        }
    }
    
    func hideLoader() {
        UIView.animate(withDuration: 0.25) {
            self.contentView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.contentView.alpha = 0.0
        } completion: { _ in
            self.removeFromSuperview()
        }
    }
}
