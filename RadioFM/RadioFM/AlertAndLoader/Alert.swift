//
//  Alert.swift
//  CarWashServiceProvider
//
//  Created by GOVIND KUMAR on 19/05/23.
//

import UIKit
enum ButtonType{
    case yesno
    case ok
}
class Alert: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var buttonNo: UIButton!
    @IBOutlet weak var buttonYes: UIButton!
    @IBOutlet weak var buttonOk: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    private var actionOnOk: (() -> Void)? = nil
    private var actionOnYes: (() -> Void)? = nil
    private var actionOnNo: (() -> Void)? = nil

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func showAlert(title: String,
                   message: String,
                   buttonType: ButtonType,
                   actionOnOk: (() -> Void)? = nil,
                   actionOnYes: (() -> Void)? = nil,
                   actionOnNo: (() -> Void)? = nil) {
        Bundle.main.loadNibNamed("Alert", owner: self, options: nil)
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(self.contentView)
        contentView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.contentView.alpha = 0.0
        self.contentView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        UIView.animate(withDuration: 0.25) {
            self.contentView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.contentView.alpha = 1.0
        }
        self.labelTitle.text = title
        self.labelMessage.text = message
        self.actionOnOk = actionOnOk
        self.actionOnYes = actionOnYes
        self.actionOnNo = actionOnNo
        if buttonType == .ok {
            self.buttonNo.isHidden = true
            self.buttonYes.isHidden = true
            self.buttonOk.isHidden = false
        } else {
            self.buttonNo.isHidden = false
            self.buttonYes.isHidden = false
            self.buttonOk.isHidden = true
        }
    }
    
    @IBAction func actionOnNo(_ sender: Any) {
        hideAlert()
        actionOnNo?()
    }
    
    @IBAction func actionOnYes(_ sender: Any) {
        hideAlert()
        actionOnYes?()
    }
    
    @IBAction func actionOnOk(_ sender: Any) {
        hideAlert()
        actionOnOk?()
    }
    
    private func hideAlert() {
        UIView.animate(withDuration: 0.25) {
            self.contentView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.contentView.alpha = 0.0
        } completion: { _ in
            self.removeFromSuperview()
        }
    }
}
