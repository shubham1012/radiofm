//
//  AlertAndLoader.swift
//  CarWashServiceProvider
//
//  Created by GOVIND KUMAR on 18/05/23.
//

import UIKit

final class AlertAndLoader: UIView {
    
    static let shared = AlertAndLoader()
    
    private var aLoader: Loader?

    func showLoader() {
        if let window = UIApplication.shared.windows.first {
            aLoader = Loader(frame: window.bounds)
            window.addSubview(aLoader!)
            aLoader?.showLoader()
        }
    }
    
    func hideLoader() {
        aLoader?.hideLoader()
    }
    
    func showAlert(title: String = "Alert",
                   message: String,
                   buttonType: ButtonType = .ok,
                   actionOnOk: (() -> Void)? = nil,
                   actionOnYes: (() -> Void)? = nil,
                   actionOnNo: (() -> Void)? = nil) {
        if let window = UIApplication.shared.windows.first {
            let aAlert = Alert(frame: window.bounds)
            window.addSubview(aAlert)
           aAlert.showAlert(title: title,
                            message: message,
                            buttonType: buttonType,
                            actionOnOk: actionOnOk,
                            actionOnYes: actionOnYes,
                            actionOnNo: actionOnNo)
        }
    }
}
