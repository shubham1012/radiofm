//
//  SocialviewController.swift
//  RadioFM
//
//  Created by GOVIND KUMAR on 29/07/23.
//

import UIKit

class SocialviewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var departmentLabel: UILabel!
    @IBOutlet weak var stationImage: UIImageView!
    var aStation: Station?

    override func viewDidLoad() {
        super.viewDidLoad()
        departmentLabel.text = "DEPARTAMENTO COMERCIAL\n" + (aStation?.contatos.dptoComercial ?? "")
        stationImage.sd_setImage(with: URL(string: aStation?.logo ?? ""))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension SocialviewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell") as? HomeTableCell
        switch indexPath.row {
        case 0:
            cell?.imageViewIcon.image = UIImage(named: "facebook")
            cell?.labelName.text = "https://www.facebook.com/" + (aStation?.contatos.facebook ?? "")
        case 1:
            cell?.imageViewIcon.image = UIImage(named: "instagram")
            cell?.labelName.text = "https://www.instagram.com/" + (aStation?.contatos.instagram ?? "")
        case 2:
            cell?.imageViewIcon.image = UIImage(named: "whatsapp")
            cell?.labelName.text = "https://wa.me/" + (aStation?.contatos.whatsapp ?? "")
        case 3:
            cell?.imageViewIcon.image = UIImage(named: "mail")
            cell?.labelName.text = "mailto:" + (aStation?.contatos.eMail ?? "")
        default:
            break
        }
        
       
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? HomeTableCell
        
        if let url = URL(string: cell?.labelName.text ?? "") {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
}
