//
//  ApiServices.swift
//  
//
//  Created by sourabh on 10/10/20.
//

import Alamofire

final class ApiService: NSObject {
    static let shared = ApiService()
    
    private override init(){
        super.init()
    }
    
    func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }

    func callServiceWithRequest(url: String, completion: @escaping (AppModel?, Error?) -> Void) {
        AlertAndLoader.shared.showLoader()
        if self.isConnectedToInternet() {
            let  url = SERVERBASEURL
            let backgroundQueue = DispatchQueue(label: "NetworkCall", qos: .background)
            backgroundQueue.async {
                AF.request(url,
                           method: .get,
                           encoding: JSONEncoding.default).response { response in
                    AlertAndLoader.shared.hideLoader()
                    switch response.result {
                    case .success(_):
                        if let jsonData = response.data {
                            do {
                                let aAccountModel = try JSONDecoder().decode(AppModel.self,
                                                                          from: jsonData)
                                completion(aAccountModel, nil)
                            } catch let error {
                                AlertAndLoader.shared.showAlert(message: error.localizedDescription)
                            }
                        } else {
                            AlertAndLoader.shared.showAlert(message: "Api Error")
                        }
                  case .failure(let error):
                        completion(nil, error)
                    }
                }
            }
        } else {
            AlertAndLoader.shared.hideLoader()
            let errorTemp = NSError(domain: "", code: -1005,
                                    userInfo: [NSLocalizedDescriptionKey: "No Internet"])
            completion(nil, errorTemp)
        }
    }
}
