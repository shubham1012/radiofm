//
//  HomeviewController.swift
//  RadioFM
//
//  Created by GOVIND KUMAR on 29/07/23.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imageViewCell: UIImageView!
    @IBOutlet weak var cellWidth: NSLayoutConstraint!
}

class HomeTableCell: UITableViewCell {
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageViewIcon: UIImageView!
}

class HomeviewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    var aAppModel: AppModel?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ApiService.shared.callServiceWithRequest(url: "") { appModel, error in
            DispatchQueue.main.async {
                if let object = appModel {
                    self.setUpUI(appModel: object)
                }
            }
        }
    }
    func setUpUI(appModel: AppModel){
        aAppModel = appModel
        self.view.backgroundColor = self.hexStringToUIColor(hex: appModel.splash.bgcolor)
        tableView.reloadData()
        collectionView.reloadData()
    }
    
    func moveToWebviewController(index: Int){
        if let viewController = self.storyboard?.instantiateViewController(identifier: "WebviewController") as? WebviewController {
            viewController.aStation = aAppModel?.stations[index]
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension HomeviewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        aAppModel?.stations.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let object = aAppModel?.stations[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell") as? HomeTableCell
        cell?.imageViewIcon.sd_setImage(with: URL(string: object?.logo ?? ""))
        cell?.labelName.text = object?.title
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.moveToWebviewController(index: indexPath.row)
    }
}

extension HomeviewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aAppModel?.banners.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"HomeCollectionCell", for:indexPath) as! HomeCollectionCell
        let object = aAppModel?.banners[indexPath.row]
        cell.imageViewCell.sd_setImage(with: URL(string: object?.img ?? ""))
        cell.cellWidth.constant = self.view.frame.size.width
        return cell
    }
}
