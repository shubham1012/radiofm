//
//  ViewController.swift
//  RadioFM
//
//  Created by GOVIND KUMAR on 29/07/23.
//

import UIKit
import SDWebImage

class SplashController: UIViewController {

    @IBOutlet weak var imageViewLogo: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        ApiService.shared.callServiceWithRequest(url: "") { appModel, error in
            DispatchQueue.main.async {
                if let object = appModel {
                    self.setUpUI(appModel: object)
                }
            }
        }
    }
    
    func setUpUI(appModel: AppModel){
        self.view.backgroundColor = self.hexStringToUIColor(hex: appModel.splash.bgcolor)
        self.imageViewLogo.sd_setImage(with: URL(string: appModel.splash.logo))
        if let time = Int(appModel.splash.time) {
            var dispatchAfter = DispatchTimeInterval.seconds(time)
            DispatchQueue.main.asyncAfter(deadline: .now() + dispatchAfter) {
                self.moveToHome()
            }
        }
    }
   
    func moveToHome(){
        if let viewController = self.storyboard?.instantiateViewController(identifier: "HomeviewController") as? HomeviewController {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

